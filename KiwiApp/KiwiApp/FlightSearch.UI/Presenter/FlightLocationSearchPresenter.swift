//
//  FlightLocationSearchPresenter.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation

class FlightLocationSearchPresenter: FlightLocationSearchPresenterDelegate {
    let repository: FlightSearchRepositoryProtocol!
    
    weak var view: FlightLocationSearchTableViewController?
    
    init() {
        let networking = KiwiNetworkProvider<FlightSearchAPI>(networkClient: URLSessionNetworkClient(session: .shared))
        self.repository = FlightSearchRepository(networking: networking)
    }
    
    func getLocations(term: String, locale: String, handler: ((LocationSearchResponse?, Error?) -> Void)?) {
        self.repository.getLocations(term: term, locale: locale) { response, error in
            self.view?.viewState = LocationViewState(locationResponse: response)
        }
    }
}
