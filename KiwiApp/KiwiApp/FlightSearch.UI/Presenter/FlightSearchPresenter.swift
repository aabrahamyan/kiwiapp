//
//  FlightSearchPresenter.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation

class FlightSearchPresenter: FlightSearchPresenterDelegate {
    let repository: FlightSearchRepositoryProtocol!
    
    weak var view: FlightSearchViewController?
    
    init() {
        let networking = KiwiNetworkProvider<FlightSearchAPI>(networkClient: URLSessionNetworkClient(session: .shared))
        self.repository = FlightSearchRepository(networking: networking)        
    }
    
    func searchFlights(iata code: String,
                       handler: ((FlightSearchResponse?, Error?) -> Void)?) {
        let today = Date()
        let fromDateString = DateUtils.dateToAPIFormat(date: today)                
        let toDate = DateUtils.appendComponentTo(date: today, by: 5, with: .day)
        let toDateString = DateUtils.dateToAPIFormat(date: toDate ?? today) // Could bring 0 results if future date is nil
        
        self.repository.searchFlights(departure: code, fromDate: fromDateString, toDate: toDateString) { response, error in
            if error == nil {
                let flights = response?.data.map { return FlightSearchViewState(flightEntity: $0)}
                self.view?.viewState = flights
            }
        }
    }
}
