//
//  FlightLocationSearchTableTableViewController.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import UIKit

protocol FlightLocationSearchPresenterDelegate {
    func getLocations(term: String, locale: String, handler: ((LocationSearchResponse?, Error?) -> Void)?)
}

class FlightLocationSearchTableViewController: UITableViewController {
    
    struct Constants {
        static let cellIdentifier = "location.cell.identifier"
    }
    
    private let presenter: FlightLocationSearchPresenter
    
    typealias ViewState = LocationViewState
    public var viewState:ViewState? {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    weak var flightSearchViewControllerDelegate: FlightSearchViewControllerProtocol?
    
    init(presenter: FlightLocationSearchPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
        presenter.view = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewState = viewState else { return 0 }
        return viewState.locations.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: Constants.cellIdentifier)
        }
        
        guard let viewState = viewState else { return cell! }
        let location = viewState.locations[indexPath.row]
        cell?.textLabel?.text = location.city.name
        cell?.detailTextLabel?.text = location.name
        
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let viewState = viewState else { return }
        let location = viewState.locations[indexPath.row]
        
        flightSearchViewControllerDelegate?.didSelect(iata: location.code)
        self.dismiss(animated: true, completion: nil)
    }
}

extension FlightLocationSearchTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        // TODO: Create Operations Queue to cancel request tasks
    }
}

extension FlightLocationSearchTableViewController: UISearchBarDelegate {
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        guard let term = searchBar.text?.trimmingCharacters(in: .whitespacesAndNewlines) else { return }
        guard let locale = Locale.current.languageCode else { return }
        self.presenter.getLocations(term: term, locale: locale, handler: nil)
    }        
}
