//
//  UIFlightSearchViewCell.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 18.04.21.
//

import UIKit

class FlightSearchViewCell: UICollectionViewCell {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var bgView: UIView!
    typealias ViewState = FlightSearchViewState
    public var viewState: ViewState? {
        didSet {
            self.updateCell()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.textColor = .white
        self.dateLabel.textColor = .white
        self.priceLabel.textColor = .white
        self.imageView.clipsToBounds = true
        self.imageView.layer.masksToBounds = true
    }
    
    public func updateImage(image: UIImage) {
        imageView.image = image
    }
    
    private func updateCell() {
        guard let viewState = viewState else { return }
        priceLabel.text = viewState.priceLabelString
        titleLabel.text = viewState.titleLabelString
        dateLabel.text = viewState.dateLabelString
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.imageView.layer.cornerRadius = 12
    }
}
