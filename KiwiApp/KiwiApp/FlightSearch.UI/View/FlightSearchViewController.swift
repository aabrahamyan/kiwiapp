//
//  ViewController.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 15.04.21.
//

import UIKit

protocol FlightSearchPresenterDelegate {
    func searchFlights(iata code: String,
                       handler: ((FlightSearchResponse?, Error?) -> Void)?)
}

protocol FlightSearchViewControllerProtocol: AnyObject {
    func didSelect(iata code: String)
}

class FlightSearchViewController: UIViewController {
    deinit {
        self.collectionView.prefetchDataSource = nil
        self.collectionView.delegate = nil
        self.collectionView.dataSource = nil
    }
    
    struct Constants {
        static let cellidentifier = "com.kiwi.flightcell"
    }
        
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    lazy var searchController: UISearchController = {
        let locationSearchTableViewController = FlightLocationSearchTableViewController(presenter: FlightLocationSearchPresenter())
        locationSearchTableViewController.flightSearchViewControllerDelegate = self
        let searchController = UISearchController(searchResultsController: locationSearchTableViewController)
        searchController.searchResultsUpdater = locationSearchTableViewController
        searchController.searchBar.placeholder = "Enter origin"
        searchController.searchBar.delegate = locationSearchTableViewController
        searchController.delegate = self
        return searchController
    }()
    
    var presenter: FlightSearchPresenter!
    
    typealias ViewState = [FlightSearchViewState]
    var viewState: ViewState? {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    private let imageLoadingQueue = OperationQueue()
    private var imageLoadOperations = [IndexPath: ImageLoadingOperation]()
    private var collectionViewFlowLayout = FlightsCollectionViewFlowLayout()
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.presenter = FlightSearchPresenter()
        self.presenter.view = self
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.searchController = searchController
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.title = "Explore"
        
        self.collectionView.prefetchDataSource = self
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.collectionViewLayout = collectionViewFlowLayout
        self.activityIndicator.hidesWhenStopped = true
    }
}

extension FlightSearchViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        DispatchQueue.main.async {
            searchController.searchResultsController?.view.isHidden = false
        }
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
        searchController.searchResultsController?.view.isHidden = false
    }
}

extension FlightSearchViewController: FlightSearchViewControllerProtocol {
    func didSelect(iata code: String) {
        self.viewState = []
        self.collectionView.reloadData()
        self.activityIndicator.startAnimating()
        self.presenter.searchFlights(iata: code) { _, _ in
            self.activityIndicator.stopAnimating()
        }
    }
}

extension FlightSearchViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let viewState = viewState else { return 0 }
        return viewState.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellidentifier, for: indexPath) as! FlightSearchViewCell
        
        if let viewState = self.viewState?[indexPath.row] {
            if let imageLoadingOperation = self.imageLoadOperations[indexPath], let image = imageLoadingOperation.image {
                cell.updateImage(image: image)
            } else {
                let imageLoadingOperation = ImageLoadingOperation(url: viewState.imageUrlString)
                imageLoadingOperation.completionHandler = { [weak self] image in
                    guard let strongSelf = self else { return }
                                        
                    strongSelf.imageLoadOperations.removeValue(forKey: indexPath)
                    
                    DispatchQueue.main.async {
                        cell.updateImage(image: image)
                    }
                }
                
                imageLoadingQueue.addOperation(imageLoadingOperation)
                imageLoadOperations[indexPath] = imageLoadingOperation
            }
            
            cell.viewState = viewState
        }
        
        return cell
    }
}

// MARK: Prefatching Images
extension FlightSearchViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            if let _ = imageLoadOperations[indexPath] { }            
            if let cellViewState = viewState?[indexPath.row] {
                let imageLoadingOperation = ImageLoadingOperation(url: cellViewState.imageUrlString)
                self.imageLoadingQueue.addOperation(imageLoadingOperation)
                self.imageLoadOperations[indexPath] = imageLoadingOperation
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cancelPrefetchingForItemsAt indexPaths: [IndexPath]) {
        for indexPath in indexPaths {
            guard let imageLoadingOperation = self.imageLoadOperations[indexPath] else { return }
            
            imageLoadingOperation.cancel()
            self.imageLoadOperations.removeValue(forKey: indexPath)
        }
    }
}
