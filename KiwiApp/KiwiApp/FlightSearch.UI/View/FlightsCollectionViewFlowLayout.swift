//
//  FlightsCollectionViewFlowLayout.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 19.04.21.
//

import UIKit

class FlightsCollectionViewFlowLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        self.minimumLineSpacing = 5
        self.minimumInteritemSpacing = 1
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureLayout() {
        self.scrollDirection = .vertical
        if let collectionView = self.collectionView {
            self.itemSize = CGSize(width: collectionView.frame.width, height: 200)
        }
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        self.configureLayout()
    }
}
