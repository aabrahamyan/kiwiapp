//
//  FlightSearchViewState.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 18.04.21.
//

import Foundation

public struct FlightSearchViewState {
    public var titleLabelString: String
    public var dateLabelString: String
    public var priceLabelString: String
    public var imageUrlString: String
    
    public init(flightEntity: FlightEntity) {
        self.titleLabelString = flightEntity.cityTo
        self.dateLabelString = FlightSearchViewState.transformDateLabel(flightEntity: flightEntity)
        self.priceLabelString = "\(flightEntity.price) €" // NO LOCALE CONVERSION, JUST FOR SIMPLICITY REASONS IN A DEMO
        self.imageUrlString = "https://images.kiwi.com/photos/600x330/" + flightEntity.imageID + ".jpg​".trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    private static func transformDateLabel(flightEntity: FlightEntity) -> String {
        let departureDate = Date(timeIntervalSinceReferenceDate: flightEntity.departureTime)
        let departureComponents = Calendar.current.dateComponents([.day], from: departureDate)
        
        guard let departureDay = departureComponents.day else { return "" }
        
        return DateUtils.dayOfTheWeek(date: departureDate) + " " + "\(departureDay)" + ", " + DateUtils.monthOfTheWeek(date: departureDate) + ", " + flightEntity.duration
    }
}
