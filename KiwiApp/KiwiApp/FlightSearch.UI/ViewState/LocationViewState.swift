//
//  LocationViewState.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 18.04.21.
//

import Foundation

public struct LocationViewState {
    
    public var locations: [LocationEntity]
    
    public init(locationResponse: LocationSearchResponse?) {
        self.locations = locationResponse?.locations ?? []
    }
}
