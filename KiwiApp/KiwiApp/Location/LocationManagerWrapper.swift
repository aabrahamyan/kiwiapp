//
//  LocationManager.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation
import CoreLocation

public class LocationManagerWrapper: NSObject {
    private let locationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    
    public var exposedLocation: CLLocation? {
        get {
            return currentLocation
        }
    }
    
    override init() {
        super.init()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
    }
}

// MARK: Update Location
extension LocationManagerWrapper: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            currentLocation = location
        }
    }
}

// MARK: Get the place
extension LocationManagerWrapper {
    func getPlace(completion: @escaping (CLPlacemark?) -> Void) {
        guard let location = currentLocation else { return }
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            guard error == nil else {
                print("*** Error in \(#function): \(error!.localizedDescription)")
                completion(nil)
                return
            }
            
            guard let placemark = placemarks?[0] else {
                print("*** Error in \(#function): placemark is nil")
                completion(nil)
                return
            }
            
            completion(placemark)
        }
    }
}
