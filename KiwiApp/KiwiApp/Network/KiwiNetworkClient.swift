//
//  KiwiNetworkClient.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 15.04.21.
//

import Foundation

public protocol KiwiNetworkClient {
    
    @discardableResult
    func send<Payload: Encodable, Result: Decodable>(request: KiwiNetworkRequest, payload: Payload, decoder: JSONDecoder, _ completionHanlder:
    @escaping (KiwiNetworkResponse<Result>) -> Void) -> URLSessionDataTask?
    
    @discardableResult
    func send<Result: Decodable>(request: KiwiNetworkRequest, decoder: JSONDecoder, _ completionHandler: @escaping
        (KiwiNetworkResponse<Result>) -> Void) -> URLSessionDataTask
    
    @discardableResult
    func send(request: KiwiNetworkRequest, _ completionHandler: @escaping (KiwiNetworkResponse<Data>) -> Void) -> URLSessionDataTask
}

public final class URLSessionNetworkClient: KiwiNetworkClient {
    
    private let session: URLSession
    
    public init(session: URLSession?) {
        if let session = session {
            self.session = session
        } else {
            self.session = URLSession(configuration: .default)
        }
    }
    
    @discardableResult
    public func send<Payload, Result>(request: KiwiNetworkRequest, payload: Payload, decoder: JSONDecoder, _ completionHanlder: @escaping (KiwiNetworkResponse<Result>) -> Void) -> URLSessionDataTask? where Payload : Encodable, Result : Decodable {
        var requestWthPayload: KiwiNetworkRequest
        
        do {
            requestWthPayload = try request.add(payload)
        } catch {
            let response = KiwiNetworkResponse<Result>(data: nil, response: nil, result: .failure(.conversionFailed(error, statusCode: nil)))
            completionHanlder(response)
            return nil
        }
        
        return self.send(request: requestWthPayload, decoder: decoder, completionHanlder)
    }
    
    public func send<Result>(request: KiwiNetworkRequest, decoder: JSONDecoder, _ completionHandler: @escaping (KiwiNetworkResponse<Result>) -> Void) -> URLSessionDataTask where Result : Decodable {
        
        return self.send(request: request) { (kiwiResponse: KiwiNetworkResponse<Data>) in
            
            switch kiwiResponse.result {
            case .failure(let error):
                let response = KiwiNetworkResponse<Result>(defaultResponse: kiwiResponse, result: .failure(error))
                completionHandler(response)
            case .success(let data):
                let decodedJson: Result
                do {
                    decodedJson = try decoder.decode(Result.self, from: data)
                } catch {
                    print(error)
                    let response = KiwiNetworkResponse<Result>(defaultResponse: kiwiResponse, result: .failure(.conversionFailed(error, statusCode: kiwiResponse.statusCode)))
                    completionHandler(response)
                    return
                }
                
                let response = KiwiNetworkResponse<Result>(defaultResponse: kiwiResponse, result: .success(decodedJson))
                completionHandler(response)
            }
        }
    }
    
    public func send(request: KiwiNetworkRequest, _ completionHandler: @escaping (KiwiNetworkResponse<Data>) -> Void) -> URLSessionDataTask {
        
        return self.send(request: request) { (kiwiResponse: DefaultKiwiResponse) in
            if let error = kiwiResponse.error {
                let kiwiResponse = KiwiNetworkResponse<Data>(data: nil,
                                                               response: kiwiResponse.response,
                                                               result: .failure(error))
                completionHandler(kiwiResponse)
                return
            }
            
            guard let data = kiwiResponse.data, data.count > 0 else {
                let kiwiResponse = KiwiNetworkResponse<Data>(data: nil, response: kiwiResponse.response, result: .failure(.conversionFailed(KiwiConversionFailedError.emptyData, statusCode: kiwiResponse.statusCode)))
                completionHandler(kiwiResponse)
                return
            }
            
            let kiwiResponse = KiwiNetworkResponse(data: data, response: kiwiResponse.response, result: .success(data))
            completionHandler(kiwiResponse)
        }
    }
    
    // MARK: Private section
    
    private final func send(request: KiwiNetworkRequest, _ completionHandler: @escaping (DefaultKiwiResponse) -> Void) -> URLSessionDataTask {
        let dataTask = session.dataTask(with: request.urlRequest) { data, response, error in
            if let error = error {
                let kiwiResponse = DefaultKiwiResponse(data: data, response: response, error: KiwiNetworkError.networkError(error))
                completionHandler(kiwiResponse)
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                let kiwiResponse = DefaultKiwiResponse(data: nil, response: nil, error: KiwiNetworkError.noResponse)
                completionHandler(kiwiResponse)
                return
            }
            
            // NOTE: Usually positive or correct responses are between these ranges, in some custom cases much higher, but I skip it here
            guard (200..<205).contains(response.statusCode) else {
                let kiwiResponse = DefaultKiwiResponse(data: nil, response: response, error: KiwiNetworkError.unexpectedStatusCode(response.statusCode))
                completionHandler(kiwiResponse)
                return
            }
            
            let kiwiResponse = DefaultKiwiResponse(data: data, response: response, error: nil)
            completionHandler(kiwiResponse)
        }
        
        dataTask.resume()
        return dataTask
    }
}
