//
//  KiwiNetworkRequest.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 15.04.21.
//

import Foundation

public struct KiwiNetworkRequest: Equatable {
    public let urlRequest: URLRequest
    
    public init(urlRequest: URLRequest) {
        self.urlRequest = urlRequest
    }
    
    public static func getRequest(url: URL,
                                  queryItems: [URLQueryItem]? = nil,
                                  httpHeaders: [String: String]? = nil) -> KiwiNetworkRequest {
        let urlRequest = getUrlRequest(url: url, queryItems: queryItems, httpHeaders: httpHeaders)
        return KiwiNetworkRequest(urlRequest: urlRequest)
    }
    
    // NOTE: In case we will need to send POST requests (not used in this example as we only hit images)
    public func addPayload(_ payload: Data) -> KiwiNetworkRequest {
        var requestWithPayload = self.urlRequest
        requestWithPayload.httpBody = payload
        return KiwiNetworkRequest(urlRequest: requestWithPayload)
    }
    
    public func add<Payload: Encodable>(_ payload: Payload) throws -> KiwiNetworkRequest {
        let encodedPayload = try JSONEncoder().encode(payload)
        return addPayload(encodedPayload)
    }
}

private extension KiwiNetworkRequest {
    
    private static func getUrlRequest(url: URL,
                                   queryItems: [URLQueryItem]? = nil,
                                   httpHeaders: [String: String]? = nil) -> URLRequest {
        var urlRequest = URLRequest(url: url)
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        if let queryItems = queryItems {
            components?.queryItems = queryItems
        }
        
        if let httpHeaders = httpHeaders {
            urlRequest.allHTTPHeaderFields = httpHeaders
        }
        
        urlRequest.url = components?.url
        
        return urlRequest
    }
}
