//
//  KiwiNetworkResponse.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 15.04.21.
//

import Foundation

public enum KiwiNetworkError: Error {
    case networkError(Error)
    case noResponse
    case unexpectedStatusCode(Int)
    case conversionFailed(Error, statusCode: Int?)
}

public enum KiwiConversionFailedError: Error {
    case emptyData
    case stringConversionFailed
}

extension KiwiNetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .networkError(let networkError):
            return "A Network Error: \(networkError.localizedDescription)"
        case .noResponse:
            return "No response from server."
        case .unexpectedStatusCode(let statusCode):
            return "Unexpected status code: \(statusCode)"
        case .conversionFailed(let conversionError, let statusCode):
            var result = "Conversion failed: \(conversionError.localizedDescription)"
            if let statusCode = statusCode {
                result.append(", response status code: \(statusCode)")
            }
            return result
        }
    }
}

public struct DefaultKiwiResponse {
    public let response: HTTPURLResponse?
    public let data: Data?
    public let error: KiwiNetworkError?
    
    var statusCode: Int? {
        return response?.statusCode
    }
    
    init(data: Data?,
         response: URLResponse?,
         error: KiwiNetworkError?) {
        
        self.data = data
        self.response = response as? HTTPURLResponse //NOTE: Because we operate with URLSessionTask
        self.error = error
    }
}

public struct KiwiNetworkResponse<Value> {
    public let response: HTTPURLResponse?
    public let data: Data?
    public let result: Result<Value, KiwiNetworkError>
    
    var statusCode: Int? {
        return response?.statusCode
    }
    
    init(data: Data?, response: URLResponse?, result: Result<Value, KiwiNetworkError>) {
        self.data = data
        self.response = response as? HTTPURLResponse
        self.result = result
    }
    
    init<Default>(defaultResponse: KiwiNetworkResponse<Default>, result: Result<Value, KiwiNetworkError>) {
        self.data = defaultResponse.data
        self.response = defaultResponse.response
        self.result = result
    }
}
