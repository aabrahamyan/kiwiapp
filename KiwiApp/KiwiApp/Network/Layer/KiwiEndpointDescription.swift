//
//  KiwiEndpointDescription.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 15.04.21.
//

import Foundation

public protocol KiwiEndpointDescription {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: [String: String]? { get }
    var body: Encodable? { get }
    var headers: [String: String]? { get }
}

public enum HTTPMethod: String {
    case post = "POST"
    case put = "PUT"
    case get = "GET"
    case delete = "DELETE"
}

