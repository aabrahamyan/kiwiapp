//
//  FlightEntity.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 16.04.21.
//

import Foundation

public struct FlightEntity: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case cityTo
        case duration = "fly_duration"
        case departureTime = "dTime"
        case imageID = "mapIdto"
        case arrivalTime = "aTime"
        case price
    }
    
    public var cityTo: String
    public var duration: String
    public var departureTime: TimeInterval
    public var arrivalTime: TimeInterval
    public var imageID: String
    public var price: Int
}
