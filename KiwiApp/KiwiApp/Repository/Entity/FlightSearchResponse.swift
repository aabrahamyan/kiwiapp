//
//  FlightSearchResponse.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation

public struct FlightSearchResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case data
    }
    
    public var data: [FlightEntity]
}
