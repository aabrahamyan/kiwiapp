//
//  LocationEntity.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation

public struct LocationEntity: Codable, Equatable {
    enum CodingKeys: String, CodingKey {
        case code
        case name
        case city
    }
    
    public var code: String
    public var name: String
    public var city: City
}

extension LocationEntity {
    public struct City: Codable, Equatable {
        enum CodingKeys: String, CodingKey {
            case name
            case code
        }
        
        public var name: String
        public var code: String
    }
}
