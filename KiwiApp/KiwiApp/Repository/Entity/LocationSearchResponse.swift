//
//  LocationSearchResponse.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 17.04.21.
//

import Foundation

public struct LocationSearchResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case locations
    }
    
    public var locations: [LocationEntity]
}
