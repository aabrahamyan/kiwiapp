//
//  FlightSearchAPI.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 16.04.21.
//

import Foundation

enum FlightSearchAPI: Equatable {
    case searchFlights(departurePlace: String, dateFromString: String, dateToString: String)
    case getLocations(term: String, locale: String)
}

extension FlightSearchAPI: KiwiEndpointDescription {
    var baseURL: URL {
        return URL(string: "https://api.skypicker.com")!
    }
    
    var path: String {
        switch self {
        case .getLocations: return "/locations"
        case .searchFlights: return "/flights"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .searchFlights, .getLocations: return .get
        }
    }
    
    var parameters: [String : String]? {
        switch self {
        case .searchFlights(let departurePlace, let dateFromString, let dateToString):
            return ["v": "3",
                    "sort": "popularity",
                    "asc": "0",
                    "locale": "en",
                    "children": "0",
                    "infants": "0",
                    "flyFrom": departurePlace,
                    "to": "anywhere",
                    "featureName": "aggregateResults",
                    "dateFrom": dateFromString,
                    "dateTo": dateToString,
                    "typeFlight": "oneway",
                    "one_per_date": "0",
                    "oneforcity": "1",
                    "wait_for_refresh": "0",
                    "adults": "2",
                    "limit": "5",
                    "partner": "picky"]
        case .getLocations(let term, let locale):
            return ["location_types": "airport",
                    "limit": "10",
                    "active_only": "true",
                    "sort": "name",
                    "term": term,
                    "locale": locale]
        }
    }
    
    var body: Encodable? {
        return nil
    }
    
    var headers: [String : String]? {
        return ["Content-Encoding": "gzip"]
    }
    
    
}
