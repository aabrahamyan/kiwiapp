//
//  FlightSearchRepository.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 16.04.21.
//

import Foundation

final class FlightSearchRepository: FlightSearchRepositoryProtocol {
    
    let networking: KiwiNetworkProvider<FlightSearchAPI>
    
    init(networking: KiwiNetworkProvider<FlightSearchAPI>) {
        self.networking = networking
    }
    
    func searchFlights(departure place: String,
                       fromDate: String,
                       toDate: String,
                       handler: @escaping (FlightSearchResponse?, Error?) -> Void) {
        
        self.networking.run(.searchFlights(departurePlace: place,
                                           dateFromString: fromDate,
                                           dateToString: toDate)) { (response: FlightSearchResponse?, error) in
            handler(response, error)
        }
    }
    
    func getLocations(term: String,
                      locale: String,
                      handler: @escaping (LocationSearchResponse?, Error?) -> Void) {
        self.networking.run(.getLocations(term: term, locale: locale)) { (response: LocationSearchResponse?, error) in
            handler(response, error)
        }
    }
}
