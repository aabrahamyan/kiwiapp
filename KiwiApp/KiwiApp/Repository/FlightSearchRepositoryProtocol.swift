//
//  FlightSearchRepositoryProtocol.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 16.04.21.
//

import Foundation

public protocol FlightSearchRepositoryProtocol {
    func searchFlights(departure place: String,
                       fromDate: String,
                       toDate: String,
                       handler: @escaping (FlightSearchResponse?, Error?) -> Void)
    
    func getLocations(term: String, locale: String, handler: @escaping (LocationSearchResponse?, Error?) -> Void)
}
