//
//  DateUtils.swift
//  KiwiApp
//
//  Created by Armen Abrahamyan on 18.04.21.
//

import Foundation

public class DateUtils {
    
    public static func dateToAPIFormat(date: Date) -> String {
        let format = DateFormatter()
        format.dateFormat = "dd/MM/yyyy"
        return format.string(from: date)
    }
    
    public static func appendComponentTo(date: Date, by number: Int, with componentType: Calendar.Component) -> Date? {
        var components = DateComponents()
        components.setValue(number, for: componentType)
        return Calendar.current.date(byAdding: components, to: date)
    }
    
    public static func dayOfTheWeek(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: date).capitalized
    }
    
    public static func monthOfTheWeek(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLL"
        return dateFormatter.string(from: date).capitalized
    }
}
